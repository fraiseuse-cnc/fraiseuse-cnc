
#define INCHES_TO_MM 25.4
// define the parameters of our machine.

#define X_STEPS_PER_MM   7.273//=calculé//7.616=empiriquement
#define X_STEPS_PER_INCH (X_STEPS_PER_MM*INCHES_TO_MM)

#define Y_STEPS_PER_MM   7.273//=calculé//7.616=empiriquement
#define Y_STEPS_PER_INCH (Y_STEPS_PER_MM*INCHES_TO_MM)

#define Z_STEPS_PER_MM   400//=calculé////412.3=empiriquement 
#define Z_STEPS_PER_INCH (Z_STEPS_PER_MM*INCHES_TO_MM)

//our maximum feedrates in units/minute

#define FAST_XY_FEEDRATE_MM 350
#define FAST_Z_FEEDRATE_MM  150
#define FAST_XY_FEEDRATE_INCH (FAST_XY_FEEDRATE_MM/INCHES_TO_MM) 
#define FAST_Z_FEEDRATE_INCH (FAST_Z_FEEDRATE_MM/INCHES_TO_MM) 

//(FAST_Z_FEEDRATE_MM/INCHES_TO_MM)


//(FAST_XY_FEEDRATE_MM/INCHES_TO_MM)

// Maximum acceleration in units/minute/second
// E.g. for 300.0 machine would accelerate to 150units/minute in 0.5sec etc.

#define MAX_ACCEL_MM 1500
#define MAX_ACCEL_INCH (MAX_ACCEL_MM/INCHES_TO_MM) ///150.0 //500.0

// Maximum change in velocity per axis - if the change in velocity at the start
// of the next move is greater than this for at least one axis, we will decelerate
// to a stop before commencing the move, otherwise we will keep going
// value is units/minute

#define MAX_DELTA_V_MM 10.0
#define MAX_DELTA_V_INCH (MAX_DELTA_V_MM/INCHES_TO_MM)

// How many temperature samples to take.  each sample takes about 100 usecs.
#define TEMPERATURE_SAMPLES 0

// The *_ENABLE_PIN signals are active high as default. Define this
// to one if they should be active low instead (e.g. if you're using different
// stepper boards).
// RepRap stepper boards are *not* inverting.
#define INVERT_ENABLE_PINS 1

// If you use this firmware on a cartesian platform where the
// stepper direction pins are inverted, set these defines to 1
// for the axes which should be inverted.
// RepRap stepper boards are *not* inverting.
#define INVERT_DIR 1 // CHANGED CM - ONLY ONE FOR ALL AXES

#define STEPPERS_ALWAYS_ON 0

/****************************************************************************************
* digital i/o pin assignment
*
* you can use the undocumented feature of Arduino - pins 14-19 correspond to analog 0-5
****************************************************************************************/

//Cartesian bot pins
//These step pins should NOT be changed if you are going to use an external joystick to manually control the machine.
#define X_STEP_PIN 4
#define Y_STEP_PIN 7
#define Z_STEP_PIN 8
//The manual controls use PORTB to quickly drive multiple stepper motors at once and avoid timing and noise issues.
//Because of this, they are fixed to these pins.

//Pin for selecting Clockwise or Counterclockwise
#define X_DIR_PIN 3
#define Y_DIR_PIN 6
#define Z_DIR_PIN 9

//Pin for disabling steppers during downtime
#define X_ENABLE_PIN 2
#define Y_ENABLE_PIN 5
#define Z_ENABLE_PIN 10

//Tool head on/off pin, used to activate and disable the cutter tool head
//#define TOOL_HEAD_PIN 5 



//#if MANUAL_ATTACH == 0  
/***************If you don't have manual controls use these pins/settings*************************/

// Set to one if endstop outputs are inverting (ie: 1 means open, 0 means closed)
// RepRap opto endstops are *not* inverting.
#define ENDSTOPS_INVERTING 0

// Optionally disable max endstops to save pins or wiring
#define ENDSTOPS_MIN_ENABLED 1
#define ENDSTOPS_MAX_ENABLED 1

#define X_MIN_PIN 19
#define X_MAX_PIN 18


#define Y_MIN_PIN 16
#define Y_MAX_PIN 17


#define Z_MIN_PIN 15
#define Z_MAX_PIN 14

//#else  
/**********************If you have manual controls use the pins below*************************/
//Hold steppers switch, used to disable and enable the steppers while paused
//#define HOLD_PIN 6  
//
////Switch for manually enabling tool head while paused
//#define TOOL_HEAD_SW_PIN 4 
//
////All limit switches connected to this analog pin. If you used a single digital pin, you wouldn't be able to back off a limit switch.
////Configured as +5V--5kohm--max_min_pin--x_min--1kohm--ymin--1kohm--zmin--1kohm--x_max--1kohm--y_max--1kohm--z_max
////Where x_min is the minumim limit switch for the x axis which connects ground to that point when triggered 
//#define MAX_MIN_PIN A0
//
////Start pause pin, used to pause to use manual controls 
////Analog pin is used in case other functions are desired (ie Fast, Slow jog modes)
//#define START_PAUSE A1
//
////X Axis left right, used to read in manual control values for x axis from 10k pot with center being no motion
//#define X_LEFT_RIGHT A2
//
////Y Axis left right, used to read in manual control values for y axis from 10k pot with center being no motion
//#define Y_LEFT_RIGHT A3
//
////Z Axis up down, used to read in manual control values for z axis from 10k pot with center being no motion
//#define Z_UP_DOWN A4
//
//#endif
/****************************end manual controls***********************************/

//extruder pins
// NOTE - USING Timer1 FOR STEPPER TIMER SO CAN'T USER PINS 9 OR 10 FOR PWM
// OUTPUT (EXTRUDER_MOTOR_SPEED_PIN, EXTRUDER_HEATER_PIN, OR EXTRUDER_FAN_PIN)
#define EXTRUDER_MOTOR_SPEED_PIN   -1
#define EXTRUDER_MOTOR_DIR_PIN     -1
#define EXTRUDER_HEATER_PIN        -1
#define EXTRUDER_FAN_PIN           -1
#define EXTRUDER_THERMISTOR_PIN    -1  //a -1 disables thermistor readings
#define EXTRUDER_THERMOCOUPLE_PIN  -1 //a -1 disables thermocouple readings
